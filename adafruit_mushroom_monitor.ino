#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <Arduino.h>
#include <IRremoteESP8266.h>
#include <IRutils.h>
#include <Timer.h>
#include <DHTesp.h>
#include "secrets.h"


Timer t;
Timer t2;
DHTesp dht;
WiFiClient espClient;
PubSubClient client(espClient);
int relayPin =14;
const char* thresholdTopic = THRESHOLD_TOPIC;
const char* modeTopic = OPERATING_MODE_TOPIC;
const char* sprinklerStateTopic = SPRINKLER_STATE_TOPIC;
const char* sprinklerOverrideTopic = SPRINKLER_OVERRIDE_TOPIC;
const char* sprinklerDurationTopic = SPRINKLER_DURATION_TOPIC;
const char* temperatureTopic = TEMPERATURE_TOPIC;
const char* humidityTopic = HUMIDITY_TOPIC;

String sprinklerOverride = "OFF";
String sprinklerState = "OFF";
String operatingMode = "Manual";
int threshold = 72;
int sprinklerDuration=30;

long lastReconnectAttempt = 0;

int previousHumidity=0;
float previousTemperature=0;
String nextState ="humidity";

void setup() {
  Serial.begin(115200);
  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin, LOW);
  oledSetup();
  randomSeed(micros());
  
  boolean wifi_status = setup_wifi();
  if (wifi_status) {
    setup_OTA();
    setup_mqtt();
  }
  dhtSetup();
    Serial.println("INITIAL TEMPERATURE: " + String(previousTemperature)+ " INITIAL HUMIDITY: " + String(previousHumidity));
  t.every(10000, publishDhtData);
  t2.every(60*60*1000, autoMode);
}

void loop() {
  t.update();
  t2.update();
  if (!client.connected()) {
    long now = millis();
    if (now - lastReconnectAttempt > 5000) {
      lastReconnectAttempt = now;
      if (reconnect()) {
        lastReconnectAttempt = 0;
      }
    }
  } else {
    client.loop();
  }
}
