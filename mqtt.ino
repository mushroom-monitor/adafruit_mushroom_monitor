const char* mqttServer = MQTT_SERVER;
const int mqttPort = MQTT_PORT;
const char* mqttUser = MQTT_USERNAME;
const char* mqttPassword = MQTT_PASSWORD;

void setup_mqtt() {
  client.setServer(mqttServer, mqttPort);
  client.setCallback(decodeMessage);
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
    String clientId = "MUSHROOM_MONITOR_";
    clientId += String(random(0xfffff), HEX);
    if (client.connect("clientId", mqttUser, mqttPassword )) {
      telemetry("connected as " + clientId);
      if (client.subscribe(thresholdTopic) && client.subscribe(modeTopic) && client.subscribe(sprinklerOverrideTopic) && client.subscribe(sprinklerDurationTopic)) {
        telemetry("Subscribed to " + String(thresholdTopic) + " Default Threshold Set To " + threshold);
        telemetry("Subscribed to " + String(modeTopic) + " Default Mode Set To " + operatingMode);
        telemetry("Subscribed to " + String(sprinklerOverrideTopic) + " Default Sprinkler Override Set To " + sprinklerOverride);
        telemetry("Subscribed to " + String(sprinklerDurationTopic) + " Default Sprinkler Override Set To " + sprinklerDuration);
      }
    } else {
      Serial.println("failed with state ");
      Serial.print(client.state());
      delay(2000);
    }
  }
}

boolean reconnect() {
  if (client.connect("clientId", mqttUser, mqttPassword )) {
    telemetry("Reconnected");
    if (!(client.subscribe(thresholdTopic) && client.subscribe(modeTopic) && client.subscribe(sprinklerOverrideTopic) && client.subscribe(sprinklerDurationTopic))) {
      client.subscribe(thresholdTopic);
      client.subscribe(modeTopic);
      client.subscribe(sprinklerOverrideTopic);
      client.subscribe(sprinklerDurationTopic);
    }
    if (client.subscribe(thresholdTopic) && client.subscribe(modeTopic) && client.subscribe(sprinklerOverrideTopic)) {
      telemetry("Re-subscribed to " + String(thresholdTopic) + " Current Threshold Set To " + threshold);
      telemetry("Re-subscribed to " + String(modeTopic) + " Current Mode Set To " + operatingMode);
      telemetry("Re-subscribed to " + String(sprinklerOverrideTopic) + " Current Sprinkler Override Set To " + sprinklerOverride);
      telemetry("Re-subscribed to " + String(sprinklerDurationTopic) + " Current Sprinkler Override Set To " + sprinklerDuration);
    }
  }
  return client.connected();
}
