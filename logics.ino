void dhtSetup()
{
  dht.setup(16, DHTesp::DHT11);
  delay(10);
  previousHumidity = dht.getHumidity();
  previousTemperature = dht.getTemperature();
}

void publishDhtData() {
  if (nextState == "humidity") {
    publishHumidity();
    nextState = "temperature";
  } else if (nextState == "temperature") {
    publishTemperature();
    nextState = "humidity";
  }
}

void publishHumidity() {
  int currentHumidity = dht.getHumidity();
  if (isnan(currentHumidity)) {
    telemetry("Failed to read Humidity from sensor!");
    return;
  } else if (currentHumidity < 100) {
    telemetry("Previous Humidity: " + String(previousHumidity) + " Current Humidity " + String(currentHumidity));
    if (previousHumidity != currentHumidity) {
      client.publish(humidityTopic, String(currentHumidity).c_str(), true);
      previousHumidity = currentHumidity;
    }
    showHumidity(String(currentHumidity));
  }
}
void publishTemperature() {
  float currentTemperature = dht.getTemperature();
  if (isnan(currentTemperature)) {
    telemetry("Failed to read Humidity from sensor!");
    return;
  } else if (currentTemperature < 100) {
    telemetry("Previous Temperature: " + String(previousTemperature) + " Current Temperature " + String(currentTemperature));
    if (previousTemperature != currentTemperature) {
      client.publish(temperatureTopic, String(currentTemperature).c_str(), true);
      previousTemperature = currentTemperature;
    }
    showTemperature(String(currentTemperature));
  }
}

void sprinklerControl(String command) {
  if (command == "ON") {
    sprinklerState = "ON";
    digitalWrite(relayPin,true);
  } else if (command == "OFF") {
    sprinklerState = "OFF";
    digitalWrite(relayPin,false);
  }
  telemetry("SPRINKLER " + sprinklerState);
  client.publish(sprinklerStateTopic, sprinklerState.c_str(), true);
}

void autoMode() {
  telemetry("Previous Humidity: " + String(previousHumidity) + " Threshold " + String(threshold));
  if (previousHumidity < threshold) {
    sprinklerControl("ON");
  }
  t.after((sprinklerDuration * 1000), [] { sprinklerControl("OFF"); });
}
