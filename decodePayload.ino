void decodeMessage(char* topic, byte* payload, unsigned int length) {
  Serial.println();
  telemetry("Message arrived in topic: " + String(topic));
  String recv_payload = "";



  for (int i = 0; i < length; i++)
  {
    recv_payload += (char)payload[i];
  }

  if (strcmp(topic, thresholdTopic) == 0) {
    int previousThreshold = threshold;
    if (isDigit(recv_payload.charAt(0))) {
      threshold = recv_payload.toInt();
    }
    telemetry("Previous Threshold: " + String(previousThreshold) + " Current Threshold: " + String(threshold));
  } else if (strcmp(topic, sprinklerDurationTopic) == 0) {
    int previousSprinklerDuration = sprinklerDuration;
    
    if (isDigit(recv_payload.charAt(0))) {
      sprinklerDuration = recv_payload.toInt();
    }
    telemetry("Previous Sprinkler Duration: " + String(previousSprinklerDuration) + " Current Sprinkler Duration: " + String(sprinklerDuration));
  } else if (strcmp(topic, modeTopic) == 0) {
    String previousMode = operatingMode;
    operatingMode = recv_payload;
    telemetry("Previous Mode: " + previousMode + " Current Mode: " + operatingMode);
    if (operatingMode == "Manual" && sprinklerOverride == "ON") {
      sprinklerControl("ON");
    } else if (operatingMode == "Manual" && sprinklerOverride == "OFF") {
      sprinklerControl("OFF");
    } else if (operatingMode == "Auto") {
      autoMode();
    }
  } else if (strcmp(topic, sprinklerOverrideTopic) == 0) {
    String previousSprinklerOverride = sprinklerOverride;
    sprinklerOverride = recv_payload;
    telemetry("Previous Sprinkler Override: " + previousSprinklerOverride + " Current Sprinkler Override: " + sprinklerOverride);
    if (operatingMode == "Manual" && sprinklerOverride == "ON") {
      sprinklerControl("ON");
    } else if (operatingMode == "Manual" && sprinklerOverride == "OFF") {
      sprinklerControl("OFF");
    }
  } else {
    telemetry("Ignored Topic");
  }
}
